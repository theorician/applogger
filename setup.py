from setuptools import setup, find_packages

setup(
    name="applogger",
    version="0.1.0",
    packages=find_packages(),
    install_requires=[],
    author="theorician",
    author_email="not given",
    description="A custom application logger",
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    url="https://gitlab.com/theorician/applogger",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
