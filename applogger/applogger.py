import logging
import os

class AppLogger(logging.Logger):
    def __init__(self, name, level=logging.INFO):
        super().__init__(name, level)
        handler = logging.StreamHandler()
        formatter = logging.Formatter(f"%(asctime)s {name}/%(levelname)s: %(message)s", "%Y-%m-%dT%H:%M:%SZ")
        handler.setFormatter(formatter)
        self.addHandler(handler)
        self.setLevel(level)

    def setup_uvicorn(self):
        import uvicorn
        log_config = uvicorn.config.LOGGING_CONFIG
        log_config["formatters"]["default"]["use_colors"] = False
        log_config["formatters"]["access"]["use_colors"] = False
        log_config["formatters"]["default"]["fmt"] = self.handlers[0].formatter._fmt
        log_config["formatters"]["access"]["fmt"] = self.handlers[0].formatter._fmt
        log_config["formatters"]["default"]["datefmt"] = self.handlers[0].formatter.datefmt
        log_config["formatters"]["access"]["datefmt"] = self.handlers[0].formatter.datefmt
        return log_config

    @classmethod
    def get(cls, name="my app name", log_level=None):
        log_level = log_level or os.environ.get("LOG_LEVEL", "INFO").upper()
        return cls(name, getattr(logging, log_level))

if __name__ == "__main__":
    logger = AppLogger.get()
    logger.info("This is an info log")
    logger.error("This is an error log")
